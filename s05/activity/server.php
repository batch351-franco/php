<?php 
session_start();

class User{
	public function login($email, $password){
		if ($email === 'johnsmith@gmail.com' && $password === '1234') {
			$_SESSION['email'] = $email;
			unset($_SESSION['login_error_message']);
		} else {
			$_SESSION['login_error_message'] = 'Incorrect username or password';
		}
	}
	public function logout($value=''){
		session_destroy();
	}
}

$user = new User();
if ($_POST['action'] === 'login'){
	$user->login($_POST['email'], $_POST['password']);
} elseif ($_POST['action'] === 'logout') {
	$user->logout();
}

header('Location: ./index.php');