<?php
class Person{
    protected $name;
    protected $age;
    protected $address;

    function __construct($name, $age, $address){
        $this->name = $name;
        $this->age = $age;
        $this->address = $address;
    }

    public function getName(){
        return $this->name;
    }

    public function getAge(){
        return $this->age;
    }

    public function getAddress(){
        return $this->address;
    }

    public function setName($name){
        $this->name = $name;
    }
    
    public function setAge($age){
        $this->age = $age;
    }
    
    public function setAddress($address){
        $this->address = $address;
    }
    
}

$person = new Person("John Smith", 30, "Quezon City, Metro Manila");


class Student extends Person{
    protected $studentId;

    function __construct($name, $age, $address, $studentId){
        parent::__construct($name, $age, $address);
        $this->studentId = $studentId;
    }

    public function getStudentId(){
        return $this->studentId;
    }

    public function setStudentId($studentId){
        $this->studentId = $studentId;
    }
}

$student = new Student("Jane Doe", 20, "Makati City, Metro Manila", "2023-1980");

class Employee extends Person{
    protected $team;
    protected $role;

    function __construct($name, $age, $address, $team, $role){
        parent::__construct($name, $age, $address);
        $this->team = $team;
        $this->role = $role;
    }

    public function getTeam(){
        return $this->team;
    }

    public function getRole(){
        return $this->role;
    }

    public function setTeam($team){
        $this->team = $team;
    }

    public function setRole($role){
        $this->role = $role;
    }
}

$employee = new Employee("Mark Blain", 35, "Pasig City, Metro Manila", "Tech Team", "Team Lead");